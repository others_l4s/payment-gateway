# Payment Gateway Integration

## Requirements
  - dotnet core hosting package for 3.1

## Requirement for Payment gateway configuration
  1. `Blockchain Payment Gateway`
        - You need to create blockchain account
        - You need to request for API key
        - After create your account you will get Xpubkey from myaccount
  2. `Skrill Payment Gateway`
        - You need to create skrill account
  3. `Paypal Payment Gateway`
        - You need to create paypal Bussiness account

## Configuration Steps

### 1. Change Appsettings.json
 ```json
 "PaymentGatewaySettings": {
    "SkrillPaymentGateway": {
      "SkrillUrl": "https://pay.skrill.com",
      "PayToEmail": "{your skrill account email address}",
      "SkrillTestUrl": "https://www.skrill.com/app/test_payment.pl",
      "SkrillStatusUrl": "{LiveURL}/Skrill/Status",
      "SkrillReturnUrl": "{LiveURL}/Skrill/Return",
      "SkrillCancelUrl": "{LiveURL}/Skrill/Cancel"
    },
    "BlockChainPaymentGateway": {
      "XPubKey": "{XPubkey from blockchain account}",
      "APIKey": "{Requested API key }",
      "CallBackUrl": "{LiveURL}/blockchain/success"
    },
    "PaypalPaymentGateway": {
      "IsTestMode": true,
      "CallBackUrl": "",
      "LiveClientId": "{Live client id}",
      "LiveClientSecret": "{ Live client secret}",
      "TestClientId": "{ Testing client id(Sandbox client id)}",
      "TestClientSecret": "{ Testing client secret (Sandbox client secret)}",
      "ReturnUrl": "{LiveURL}/PayPal/Return",
      "CancelUrl": "{LiveURL}/PayPal/Cancel"
    }
  },
 ```

### 2. Run Website
- Run website or Host website to IIS
- For Host to IIS
    - open command prompt in `PaymentGateway\PaymentGatewayIntegration` folder
    - Run `dotnet publish` command
    - host `bin\Debug\netcoreapp2.2\publish` folder to IIS
    - if you already have publish folder than directly host that folder to site

### 3. How to Use