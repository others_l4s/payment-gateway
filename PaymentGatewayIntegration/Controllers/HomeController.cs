﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Info.Blockchain.API.Receive;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PaymentGatewayIntegration.Common;
using PaymentGatewayIntegration.Models;
using PaymentGatewayIntegration.Services;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;
using PayPalHttp;

namespace PaymentGatewayIntegration.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;
		private readonly ISkrillPaymentService _skrillPaymentService;
		private readonly PaymentGatewaySettings _paymentGatewaySettings;
		public HomeController(ILogger<HomeController> logger, ISkrillPaymentService skrillPaymentService, IOptions<PaymentGatewaySettings> paymentGatewaySettings)
		{
			_logger = logger;
			_skrillPaymentService = skrillPaymentService;
			_paymentGatewaySettings = paymentGatewaySettings.Value;
		}

		public IActionResult Index()
		{
			//var client = new Receive();
			//string xpub = "xpub6CrqqAuX7Z2GCfrkDY4qvAwyWuCDYXeuyuUzLVem8YUWCb47YWSYmkahZ5rXENmzqSVrT5F77AUAV6hy8tg5iA562mW4W9Ltotm6LCknx3J";
			//string key = "40e77a7a-6a3c-4373-8395-652265ca458b";
			//string callbackurl = "https://www.lane4solution.com/home/bitcoinsuccess";
			//callbackurl = WebUtility.UrlEncode(callbackurl);
			//var response = await client.GenerateAddressAsync(xpub, callbackurl, key, null);
			//return View(response);
			//var response = await CreateOrder();
			//string response = await _skrillPaymentService.ProccessPayment();
			//if (!string.IsNullOrEmpty(response))
			//{
			//	return Redirect(_paymentGatewaySettings.SkrillPaymentGateway.SkrillUrl + "/?sid=" + response);
			//}
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		
	}
}
