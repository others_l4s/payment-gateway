﻿namespace PaymentGatewayIntegration.Common
{
	public class PaymentGatewaySettings
	{
		public SkrillPaymentGateway SkrillPaymentGateway { get; set; }
		public BlockChainPaymentGateway BlockChainPaymentGateway { get; set; }
		public PaypalPaymentGateway PaypalPaymentGateway { get; set; }
	}

	public class SkrillPaymentGateway
	{
		public string SkrillUrl { get; set; }
		public string SkrillTestUrl { get; set; }
		public string PayToEmail { get; set; }
		public string SkrillStatusUrl { get; set; }
		public string SkrillReturnUrl { get; set; }
		public string SkrillCancelUrl { get; set; }
	}

	public class BlockChainPaymentGateway
	{
		public string XPubKey { get; set; }
		public string APIKey { get; set; }
		public string CallBackUrl { get; set; }
	}

	public class PaypalPaymentGateway
	{
		public bool IsTestMode { get; set; }
		public string CallBackUrl { get; set; }
		public string LiveClientId { get; set; }
		public string LiveClientSecret { get; set; }
		public string TestClientId { get; set; }
		public string TestClientSecret { get; set; }

		public string ReturnUrl { get; set; }
		public string CancelUrl { get; set; }
	}
}
