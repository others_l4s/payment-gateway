﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentGatewayIntegration.Models;
using PaymentGatewayIntegration.Services;

namespace PaymentGatewayIntegration.Controllers
{
	public class BlockchainController : Controller
	{
		private readonly IBlockChainService _blockChainService;
		public BlockchainController(IBlockChainService blockChainService)
		{
			_blockChainService = blockChainService;
		}

		public async Task<IActionResult> Index()
		{
			var model = new BlockchainModel();
			var response = await _blockChainService.GetBTCValue();
			if(response != null)
			{
				model.MarketPriceUsd = response.MarketPriceUsd;
			}
			return View(model);
		}

		public async Task<IActionResult> ProcessPayment()
		{
			var response = await _blockChainService.ProccessPayment();
			return View(response);
		}

		public string Success()
		{
			var queryParams = HttpContext.Request.Query;
			foreach (var item in queryParams)
			{
				Console.WriteLine(item.Key + " " + item.Value);
			}

			//var invoice_id = HttpContext.Request.Query["invoice_id"];
			//var transaction_hash = HttpContext.Request.Query["transaction_hash"];
			//var value = HttpContext.Request.Query["value"];
			//var address = HttpContext.Request.Query["address"];
			//var secret = HttpContext.Request.Query["secret"];
			//var confirmations = HttpContext.Request.Query["confirmations"];
			var status = "*ok*";
			//if(confirmations > 4)
			return status;
		}

		[Route("Blockchain/AddressDetails/{address}")]
		public async Task<IActionResult> AddressDetails(string address)
        {
			var response = await _blockChainService.GetAddressDetails(address);
			return View(response);
		}
	}
}
