﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Info.Blockchain.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace PaymentGatewayIntegration.Services
{
	public interface IBlockChainService
	{
		Task<ReceivePaymentResponse> ProccessPayment();
		Task<StatisticsResponse> GetBTCValue();

		Task<Address> GetAddressDetails(string address);
	}
}
