﻿using PaymentGatewayIntegration.Models;
using PayPalCheckoutSdk.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGatewayIntegration.Services
{
	public interface IPaypalService
	{
		Task<Order> ProccessPayment(PaypalModel model);

		Task<Order> ProccessPaymentApproval(string OrderId);
	}
}
