﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using PaymentGatewayIntegration.Common;
using PaymentGatewayIntegration.Models;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;
using PayPalHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HttpResponse = PayPalHttp.HttpResponse;

namespace PaymentGatewayIntegration.Services
{
	public class PaypalService : IPaypalService
	{
		private readonly PaypalPaymentGateway _paymentGatewaySettings;
		private readonly IHttpContextAccessor _httpContextAccessor;
		public PaypalService(IOptions<PaymentGatewaySettings> paymentGatewaySettings, IHttpContextAccessor httpContextAccessor)
		{
			_paymentGatewaySettings = paymentGatewaySettings.Value.PaypalPaymentGateway;
			_httpContextAccessor = httpContextAccessor;
		}

		public async Task<Order> ProccessPayment(PaypalModel model)
		{
			string redirectUrl = "";
			model.TransactionId = "PP_" + new Random().Next(10000, 99999).ToString();
			var response = await CreateOrder(model);
			Order order = response.Result<Order>();
			return order;

			//if (order != null)
			//{
			//	_httpContextAccessor.HttpContext.Session.SetString("PaypalOrderId", order.Id);
			//	foreach (var linkDescription in order.Links)
			//	{
			//		if ("approve".Equals(linkDescription.Rel))
			//		{
			//			redirectUrl = linkDescription.Href;
			//		}
			//	}
			//}
			//return redirectUrl;
		}

		public async Task<Order> ProccessPaymentApproval(string OrderId)
		{
			string redirectUrl = "";
			var response = await CaptureOrderPayment(OrderId);
			var order = response.Result<Order>();
			return order;
		}

		public async Task<HttpResponse> CaptureOrderPayment(string OrderId)
		{
			var request = new OrdersCaptureRequest(OrderId);
			request.Prefer("return=representation");
			request.RequestBody(new OrderActionRequest());
			if (_paymentGatewaySettings.IsTestMode)
			{
				var client = new PayPalHttpClient(new SandboxEnvironment(_paymentGatewaySettings.TestClientId, _paymentGatewaySettings.TestClientSecret));
				return await client.Execute(request);
			}
			else
			{
				var client = new PayPalHttpClient(new LiveEnvironment(_paymentGatewaySettings.LiveClientId, _paymentGatewaySettings.LiveClientSecret));
				return await client.Execute(request);
			}
			//var response = await PayPalClient.client().Execute(request);

			//if (debug)
			//{
			//	var result = response.Result<Order>();
			//	Console.WriteLine("Status: {0}", result.Status);
			//	Console.WriteLine("Order Id: {0}", result.Id);
			//	Console.WriteLine("Intent: {0}", result.CheckoutPaymentIntent);
			//	Console.WriteLine("Links:");
			//	foreach (LinkDescription link in result.Links)
			//	{
			//		Console.WriteLine("\t{0}: {1}\tCall Type: {2}", link.Rel, link.Href, link.Method);
			//	}
			//	Console.WriteLine("Capture Ids: ");
			//	foreach (PurchaseUnit purchaseUnit in result.PurchaseUnits)
			//	{
			//		foreach (Capture capture in purchaseUnit.Payments.Captures)
			//		{
			//			Console.WriteLine("\t {0}", capture.Id);
			//		}
			//	}
			//	AmountWithBreakdown amount = result.PurchaseUnits[0].AmountWithBreakdown;
			//}
			//return response;
		}

		private async Task<HttpResponse> CreateOrder(PaypalModel model)
		{
			var request = new OrdersCreateRequest();
			request.Prefer("return=representation");
			request.RequestBody(BuildRequestBodyForOrder(model));
			if (_paymentGatewaySettings.IsTestMode)
			{
				var client = new PayPalHttpClient(new SandboxEnvironment(_paymentGatewaySettings.TestClientId, _paymentGatewaySettings.TestClientSecret));
				return await client.Execute(request);
			}
			else
			{
				var client = new PayPalHttpClient(new LiveEnvironment(_paymentGatewaySettings.LiveClientId, _paymentGatewaySettings.LiveClientSecret));
				return await client.Execute(request);
			}
		}

		private OrderRequest BuildRequestBodyForOrder(PaypalModel model)
		{
			var order = new OrderRequest()
			{
				CheckoutPaymentIntent = "CAPTURE",
				PurchaseUnits = new List<PurchaseUnitRequest>()
				{
					new PurchaseUnitRequest()
					{
						ReferenceId = model.TransactionId,
						AmountWithBreakdown = new AmountWithBreakdown()
						{
							CurrencyCode = model.Currency,
							Value = model.Amount.ToString()
						},
						// Pass items here for Ecommerce website
						//Items = new List<Item>
						//{
						//	new Item
						//	{
						//		Name = "T-shirt",
						//		Description = "Green XL",
						//		Sku = "sku01",
						//		UnitAmount = new Money
						//		{
						//			CurrencyCode = "USD",
						//			Value = "90.00"
						//		},
						//		Tax = new Money
						//		{
						//			CurrencyCode = "USD",
						//			Value = "10.00"
						//		},
						//		Quantity = "1",
						//		Category = "PHYSICAL_GOODS"
						//	},
						//	new Item
						//	{
						//		Name = "Shoes",
						//		Description = "Running, Size 10.5",
						//		Sku = "sku02",
						//		UnitAmount = new Money
						//		{
						//			CurrencyCode = "USD",
						//			Value = "45.00"
						//		},
						//		Tax = new Money
						//		{
						//			CurrencyCode = "USD",
						//			Value = "5.00"
						//		},
						//		Quantity = "2",
						//		Category = "PHYSICAL_GOODS"
						//	}
						//},
						//ShippingDetail = new ShippingDetail
						//{
						//	Name = new Name
						//	{
						//		FullName = "John Doe"
						//	},
						//	AddressPortable = new AddressPortable
						//	{
						//		AddressLine1 = "123 Townsend St",
						//		AddressLine2 = "Floor 6",
						//		AdminArea2 = "San Francisco",
						//		AdminArea1 = "CA",
						//		PostalCode = "94107",
						//		CountryCode = "US"
						//	}
						//}
					}
				},
				ApplicationContext = new ApplicationContext()
				{
					ReturnUrl = _paymentGatewaySettings.ReturnUrl,
					CancelUrl = _paymentGatewaySettings.CancelUrl
				}
			};
			return order;
		}
	}
}
