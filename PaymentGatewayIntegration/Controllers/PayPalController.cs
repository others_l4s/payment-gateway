﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaymentGatewayIntegration.Models;
using PaymentGatewayIntegration.Services;
using PayPalCheckoutSdk.Orders;

namespace PaymentGatewayIntegration.Controllers
{
    public class PayPalController : Controller
    {
        private readonly IPaypalService _paypalService;
        public PayPalController(IPaypalService paypalService)
        {
            _paypalService = paypalService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> ProcessPayment(PaypalModel model)
        {
            var order = await _paypalService.ProccessPayment(model);
            var redirectUrl = string.Empty;
            if (order != null)
            {
                HttpContext.Session.SetString("PaypalOrderId", order.Id);
                foreach (var linkDescription in order.Links)
                {
                    if ("approve".Equals(linkDescription.Rel))
                    {
                        redirectUrl = linkDescription.Href;
                    }
                }
            }
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }
            return View();
        }

        public async Task<IActionResult> Return()
        {
            string payerId = Request.Query["PayerID"];
            string token = Request.Query["Token"];
            string orderId = HttpContext.Session.GetString("PaypalOrderId");
            var response = await _paypalService.ProccessPaymentApproval(orderId);
            if (response != null)
            {
                return View("Success", response);
            }
            return View("Failed");
        }
    }
}
