﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PaymentGatewayIntegration.Common;
using PaymentGatewayIntegration.Models;
using PaymentGatewayIntegration.Services;

namespace PaymentGatewayIntegration.Controllers
{
	public class SkrillController : Controller
	{
		private readonly ISkrillPaymentService _skrillPaymentService;
		private readonly PaymentGatewaySettings _paymentGatewaySettings;
		public SkrillController(ISkrillPaymentService skrillPaymentService, IOptions<PaymentGatewaySettings> paymentGatewaySettings)
		{
			_skrillPaymentService = skrillPaymentService;
			_paymentGatewaySettings = paymentGatewaySettings.Value;
		}
		public IActionResult Index()
		{
			return View();
		}

		public async Task<IActionResult> ProcessPayment(SkrillModel model)
		{

			string response = await _skrillPaymentService.ProccessPayment(model);
			if (!string.IsNullOrEmpty(response))
			{
				return Redirect(_paymentGatewaySettings.SkrillPaymentGateway.SkrillUrl + "/?sid=" + response);
			}
			return View();
		}

		public ActionResult Status()
		{
			var requestKeys = Request.Form;
			foreach (var item in requestKeys)
			{
				Console.WriteLine(item.Key + " " + item.Value);
			}

			//var status = Request.Form["status"];
			////Status of the transaction: -2 failed / 2 processed / 0 pending / -1 cancelled

			//var failedReason = Request.Form["failed_reason_code"];
			//var paymentType = Request.Form["payment_type"];
			//var pay_to_email = Request.Form["pay_to_email"];
			//var pay_from_email = Request.Form["pay_from_email"];
			//var merchant_id = Request.Form["merchant_id"];
			//var transaction_id = Request.Form["transaction_id"];
			//var mb_transaction_id = Request.Form["mb_transaction_id"];
			//var mb_amount = Request.Form["mb_amount"];
			//var mb_currency = Request.Form["mb_currency"];

			return View();
		}

		public ActionResult Cancel()
		{
			return View();
		}

		public ActionResult Return()
		{
			//var requestKeys = Request.Form.Keys;

			//string[] keys = new string[requestKeys.Count];
			//requestKeys.CopyTo(keys, 0);
			//Array.Sort(keys);

			var queryParams = HttpContext.Request.Query;
			foreach (var item in queryParams)
			{
				Console.WriteLine(item.Key + " " + item.Value);
			}

			//transaction_id SK_73809
			//msid 904A5C3A8323260881404EBB271BE24D
			return View();
		}

	}
}
