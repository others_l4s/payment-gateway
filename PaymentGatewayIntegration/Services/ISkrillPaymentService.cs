﻿using PaymentGatewayIntegration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGatewayIntegration.Services
{
	public interface ISkrillPaymentService
	{
		Task<string> ProccessPayment(SkrillModel skrillModel);
	}
}
