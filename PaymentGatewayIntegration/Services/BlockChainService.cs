﻿using Info.Blockchain.API.BlockExplorer;
using Info.Blockchain.API.Models;
using Info.Blockchain.API.Receive;
using Info.Blockchain.API.Statistics;
using Microsoft.Extensions.Options;
using PaymentGatewayIntegration.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace PaymentGatewayIntegration.Services
{
	public class BlockChainService : IBlockChainService
	{
		private readonly BlockChainPaymentGateway _paymentGatewaySettings;
		public BlockChainService(IOptions<PaymentGatewaySettings> paymentGatewaySettings)
		{
			_paymentGatewaySettings = paymentGatewaySettings.Value.BlockChainPaymentGateway;
		}

		public async Task<ReceivePaymentResponse> ProccessPayment()
		{
			var invoiceId = "BC_" + new Random().Next(10000, 99999).ToString();
			var callbackUrl = _paymentGatewaySettings.CallBackUrl + "?invoice_id=" + invoiceId;
			var response = await new Receive().GenerateAddressAsync(
				_paymentGatewaySettings.XPubKey,
				WebUtility.UrlEncode(callbackUrl),
				_paymentGatewaySettings.APIKey,
				null);
			return response;
		}

		public async Task<StatisticsResponse> GetBTCValue()
		{
			return await new StatisticsExplorer().GetStatsAsync();
		}

		public async Task<Address> GetAddressDetails(string address)
        {
			return await new BlockExplorer().GetAddressDetailsAsync(address);
		}
	}
}
