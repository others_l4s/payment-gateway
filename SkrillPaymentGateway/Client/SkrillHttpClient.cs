﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SkrillPaymentGateway.Client
{
	public class SkrillHttpClient
	{
		private const string BASE_URI = "https://pay.skrill.com";
		private const int TIMEOUT_MS = 100000;
		private readonly HttpClient httpClient;

		public SkrillHttpClient(string uri = BASE_URI)
		{
			httpClient = new HttpClient
			{
				BaseAddress = new Uri(uri),
				Timeout = TimeSpan.FromMilliseconds(SkrillHttpClient.TIMEOUT_MS)
			};
		}

		public async Task<T> GetAsync<T>(string route, string queryString = null, Func<string, T> customDeserialization = null)
		{
			if (route == null)
			{
				throw new ArgumentNullException(nameof(route));
			}

			//if (queryString != null && queryString.Count > 0)
			//{
			//	int queryStringIndex = route.IndexOf('?');
			//	if (queryStringIndex >= 0)
			//	{
			//		//Append to querystring
			//		string queryStringValue = queryStringIndex.ToString();
			//		//replace questionmark with &
			//		queryStringValue = "&" + queryStringValue.Substring(1);
			//		route += queryStringValue;
			//	}
			//	else
			//	{
			//		route += queryString.ToString();
			//	}
			//}
			HttpResponseMessage response = await httpClient.GetAsync(route);
			string responseString = await ValidateResponse(response);
			var responseObject = JsonConvert.DeserializeObject<T>(responseString);
			return responseObject;
		}

		public async Task<string> PostAsync(string route, Dictionary<string, string> postObject)
		{
			if (route == null)
			{
				throw new ArgumentNullException(nameof(route));
			}
			HttpContent httpContent = new FormUrlEncodedContent(postObject);
			HttpResponseMessage response = await httpClient.PostAsync(route, httpContent);
			string responseString = await this.ValidateResponse(response);
			return responseString;
		}

		private async Task<string> ValidateResponse(HttpResponseMessage response)
		{
			if (response.IsSuccessStatusCode)
			{
				string responseString = await response.Content.ReadAsStringAsync();
				return responseString;
			}
			string responseContent = await response.Content.ReadAsStringAsync();
			throw new Exception(response.ReasonPhrase + ": " + responseContent);
		}

		public void Dispose()
		{
			httpClient.Dispose();
		}	
	}
}
