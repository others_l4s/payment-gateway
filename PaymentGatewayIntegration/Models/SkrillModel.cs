﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGatewayIntegration.Models
{
	public class SkrillModel
	{
		[Required]
		public decimal Amount { get; set; }

		public string Currency { get; set; }

		public string TransactionId { get; set; }

		[Required]
		[EmailAddress]
		public string Email { get; set; }

		[Required]
		public string FirstName { get; set; }

		[Required]
		public string LastName { get; set; }

		public SkrillModel()
		{
			this.Currency = "USD";
		}

	}
}
