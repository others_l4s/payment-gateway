﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace SkrillPaymentGateway.Model
{
	[DataContract]
	public class SkrillPaymentRequest
	{
		[DataMember(Name = "pay_to_email", EmitDefaultValue = false)]
		public string PayToEmail;

		[DataMember(Name = "status_url", EmitDefaultValue = false)]
		public string StatusUrl;

		[DataMember(Name = "language", EmitDefaultValue = false)]
		public string Language;

		[DataMember(Name = "amount", EmitDefaultValue = false)]
		public string Amount;

		[DataMember(Name = "currency", EmitDefaultValue = false)]
		public string Currency;


		[DataMember(Name = "transaction_id", EmitDefaultValue = false)]
		public string TransactionId;

		[DataMember(Name = "return_url", EmitDefaultValue = false)]
		public string ReturnUrl;


		[DataMember(Name = "cancel_url", EmitDefaultValue = false)]
		public string CancelUrl;


		[DataMember(Name = "prepare_only", EmitDefaultValue = false)]
		public string PrepareOnly;
	}
}
