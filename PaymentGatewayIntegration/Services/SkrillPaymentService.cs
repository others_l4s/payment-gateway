﻿using Microsoft.Extensions.Options;
using PaymentGatewayIntegration.Common;
using PaymentGatewayIntegration.Models;
using SkrillPaymentGateway.Client;
using SkrillPaymentGateway.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGatewayIntegration.Services
{
	public class SkrillPaymentService : ISkrillPaymentService
	{
		private readonly PaymentGatewaySettings _paymentGatewaySettings;
		public SkrillPaymentService(IOptions<PaymentGatewaySettings> paymentGatewaySettings)
		{
			_paymentGatewaySettings = paymentGatewaySettings.Value;
		}

		public async Task<string> ProccessPayment(SkrillModel skrillModel)
		{
			var skrillPaymentGatewaySettings = _paymentGatewaySettings.SkrillPaymentGateway;
			var skrillHttpClient = new SkrillHttpClient(skrillPaymentGatewaySettings.SkrillUrl);
			var request = new SkrillPaymentRequest()
			{
				Amount = "39.68",
				Currency = "INR",
				Language = "",
				PayToEmail = _paymentGatewaySettings.SkrillPaymentGateway.PayToEmail,
				PrepareOnly = "1",
				StatusUrl = "https://example.com/process_payment.cgi",
				CancelUrl = "https://example.com/process_payment.cgi",
				ReturnUrl = "https://example.com/process_payment.cgi",
				TransactionId = "4654646464"
			};
			var dict = new Dictionary<string, string>();
			dict.Add("Content-Type", "application/x-www-form-urlencoded");
			dict.Add("pay_to_email", _paymentGatewaySettings.SkrillPaymentGateway.PayToEmail);
			dict.Add("prepare_only", "1");
			dict.Add("currency", "USD");
			dict.Add("amount", skrillModel.Amount.ToString());
			dict.Add("status_url", skrillPaymentGatewaySettings.SkrillStatusUrl);
			dict.Add("return_url", skrillPaymentGatewaySettings.SkrillReturnUrl);
			dict.Add("cancel_url", skrillPaymentGatewaySettings.SkrillCancelUrl);
			dict.Add("transaction_id", "SK_"+ new Random().Next(10000, 99999).ToString());

			return await skrillHttpClient.PostAsync("", dict);
		}

	}
}
